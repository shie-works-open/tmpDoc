# Gitlabページのテスト


## Venvを利用してるので、Venvの実行方法
```bash
set:C:\git\doctest\

# venvを起動
cd C:\git\doctest\
Set-ExecutionPolicy RemoteSigned -Scope Process
E:\git\venv\docVenv\Scripts\Activate.ps1
```
## コマンド

### ローカルでのテスト

```bash
mkdocs serve
```

## 細かい内容はZennに記載した


