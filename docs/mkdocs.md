# MKDocksの使い方



## フォルダに日本語名を付けたい

フォルダ直下に`.pages`というファイルを作成する

`.pages`の中身
```
title: <日本語のタイトル>
```
他にもいろいろできるけど、よく解らないので、公式ドキュメントを見てください。

[https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin)