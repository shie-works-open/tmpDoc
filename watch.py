from watchdog.observers.polling import PollingObserver
from watchdog.events import FileSystemEventHandler
import subprocess
import os
import time


# os.system(R'C:\"Program Files (x86)"\WinSCP\WinSCP.exe sftp://toami_sys:toami0910@192.168.10.200 /console /command synchronize remote "C:\git\doc-toami\site" "/var/www/document"')

def sync():
    print('build start')
    os.system(R'C:\git\venv\docVenv\Scripts\mkdocs build')
    print('sync start')
    cmd = R'C:\"Program Files (x86)"\WinSCP\WinSCP.exe '
    cmd += R'/ini=nul /log=test.log /script=C:\git\doc-toami\scpScript'
    # cmd += R'/console  /script=C:\git\doc-toami\scpScript'
    process = subprocess.Popen(cmd,shell=True, encoding="shift-jis")
    print(f"Now Syncing... {process.pid}")
    while process.poll() is None:
        print("waiting......")
        time.sleep(2)
    print(f"Finished Syncing... {process.pid}")




#ファイル／フォルダの変化があった時に呼ばれるイベントハンドラ
class EventHandler(FileSystemEventHandler):
    # どのイベントでもこの関数が実行される
    def on_any_event(self,e):
        print(f"{e.is_directory} : {e.event_type} : {e.src_path}")

        sync()
        

#ファイル／フォルダの監視を開始
observer = PollingObserver()
observer.schedule(EventHandler(), path='./docs/', recursive=True)
observer.start()

ary=['mkdocs','serve']
process = subprocess.Popen(ary,shell=True)



#監視のためのループ処理
while True:
    time.sleep(1)